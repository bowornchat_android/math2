package buu.bowornchat.math1

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import buu.bowornchat.math1.databinding.FragmentGameBinding
import buu.bowornchat.math1.databinding.FragmentPlusBinding
import buu.bowornchat.math1.databinding.FragmentSelectBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [selectFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class selectFragment : Fragment() {
    private lateinit var viewModel: selectViewModel
    private lateinit var binding: FragmentSelectBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_select,container,false)
        viewModel = ViewModelProvider(this).get(selectViewModel::class.java)
        viewModel.score = selectFragmentArgs.fromBundle(requireArguments()).allScore
        binding.apply {
            allScore.text = viewModel.score.toString()
            plusbutton.setOnClickListener{
                plusGame()
            }
            minusbutton.setOnClickListener{
                minusGame()
            }
            multiplyButton.setOnClickListener{
                multiplyGame()
            }

        }
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.
        onNavDestinationSelected(item,requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }
    private fun plusGame() {

        val action = selectFragmentDirections.actionSelectFragmentToPlusFragment()
        action.highScore = viewModel.score
        NavHostFragment.findNavController(this).navigate(action)
    }
    private fun minusGame() {

        val action = selectFragmentDirections.actionSelectFragmentToMinusFragment()
        action.highScore = viewModel.score
        NavHostFragment.findNavController(this).navigate(action)
    }
    private fun multiplyGame() {

        val action = selectFragmentDirections.actionSelectFragmentToMultiplyFragment()
        action.highScore = viewModel.score
        NavHostFragment.findNavController(this).navigate(action)
    }



    }



