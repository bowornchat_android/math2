package buu.bowornchat.math1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import buu.bowornchat.math1.databinding.FragmentMinusBinding
import buu.bowornchat.math1.databinding.FragmentPlusBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [minusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class minusFragment : Fragment() {
    private lateinit var viewModel: GameViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var binding = DataBindingUtil.inflate<FragmentMinusBinding>(
            inflater,
            R.layout.fragment_minus, container, false
        )
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)
        viewModel.allScore = minusFragmentArgs.fromBundle(requireArguments()).highScore
        binding.gameViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer<Boolean> { hasFinished ->
            if (hasFinished) gameFinished()
        })
        binding.apply {
            round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            btnNumber1.setOnClickListener {
                if (checkTrue(txtNum1, txtNum2, btnNumber1)) {
                    txtResult.setText(R.string.corect)
                    viewModel.Win(txtScore)
                } else {
                    txtResult.setText(R.string.incorect)
                    viewModel.Lose(txtScore)
                }
                round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            }
            btnNumber2.setOnClickListener {
                if (checkTrue(txtNum1, txtNum2, btnNumber2)) {
                    txtResult.setText(R.string.corect)
                    viewModel.Win(txtScore)
                } else {
                    txtResult.setText(R.string.incorect)
                    viewModel.Lose(txtScore)
                }
                round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            }
            btnNumber3.setOnClickListener {
                if (checkTrue(txtNum1, txtNum2, btnNumber3)) {
                    txtResult.setText(R.string.corect)
                    viewModel.Win(txtScore)
                } else {
                    txtResult.setText(R.string.incorect)
                    viewModel.Lose(txtScore)
                }
                round(txtNum1, txtNum2, btnNumber1, btnNumber2, btnNumber3)
            }
            endButton.setOnClickListener { gameFinished() }
        }
        return binding.root
    }

    fun round(
        txtnumber1: TextView,
        txtnumber2: TextView,
        btnNumber1: Button,
        btnNumber2: Button,
        btnNumber3: Button
    ) {
        txtnumber1.text = viewModel.randomNumber().toString()
        txtnumber2.text = viewModel.randomNumber().toString()
        val result =
            Integer.parseInt(txtnumber1.text.toString()) - Integer.parseInt(txtnumber2.text.toString())
        when (viewModel.randomButton()) {
            0 -> {
                btnNumber1.text = result.toString()
                btnNumber2.text = (result + 1).toString()
                btnNumber3.text = (result + 2).toString()
            }
            1 -> {
                btnNumber1.text = (result - 1).toString()
                btnNumber2.text = result.toString()
                btnNumber3.text = (result + 1).toString()
            }
            2 -> {
                btnNumber1.text = (result - 2).toString()
                btnNumber2.text = (result - 1).toString()
                btnNumber3.text = result.toString()
            }
        }
    }

    fun checkTrue(txtnumber1: TextView, txtnumber2: TextView, btnNumber: Button): Boolean {
        val result =
            Integer.parseInt(txtnumber1.text.toString()) - Integer.parseInt(txtnumber2.text.toString())
        if (result == Integer.parseInt(btnNumber.text.toString())) {
            return true
        }
        return false
    }
    private fun gameFinished() {
        Toast.makeText(activity, "Game has just finished", Toast.LENGTH_SHORT).show()
        viewModel.checkHighScore()
        val action = minusFragmentDirections.actionMinusFragmentToSelectFragment()
        action.allScore = viewModel.allScore
        NavHostFragment.findNavController(this).navigate(action)
        viewModel.onGameFinishComplete()
    }
}